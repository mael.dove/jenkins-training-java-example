# Jenkins Training - Java Example

[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Example Java and Maven project with Jenkins pipeline as code

## License

See [License](LICENSE.md).
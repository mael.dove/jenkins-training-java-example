package com.example.fizzbuzz;

public final class Fizzbuzzer {
	public String fizzbuzz(long n)
	{
		StringBuilder sb = new StringBuilder();
		if (n % 3 == 0) {
			sb.append("Fizz");
		}
		if (n % 5 == 0) {
			sb.append("Buzz");
		}
		if (sb.length() == 0) {
			sb.append(n);
		}
		return sb.toString();
	}
}

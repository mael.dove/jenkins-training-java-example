package com.example.fizzbuzz;

import java.util.stream.LongStream;

public class Main {
    public static void main(String[] args)
    {
		Fizzbuzzer f = new Fizzbuzzer();
		LongStream.rangeClosed(1, 100).forEach(i -> {
			System.out.println(f.fizzbuzz(i));
		});
    }
}

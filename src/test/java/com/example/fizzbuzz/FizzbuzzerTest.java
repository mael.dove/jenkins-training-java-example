package com.example.fizzbuzz;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

public class FizzbuzzerTest {
	private Fizzbuzzer fizzbuzzer;

	@BeforeEach
	void setUp() {
		fizzbuzzer = new Fizzbuzzer();
	}

	@Test
	void one() {
		 assertEquals("1", fizzbuzzer.fizzbuzz(1));
	}

	@Test
	void two() {
		 assertEquals("2", fizzbuzzer.fizzbuzz(2));
	}

	@Test
	void fifteen() {
		 assertEquals("FizzBuzz", fizzbuzzer.fizzbuzz(15));
	}
}
